﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FireBullets : MonoBehaviour {
   


    public int amount = 6;
    public float fireRate = .5f;

    public GameObject bullet;

    List<GameObject> bullets; 

	// Use this for initialization
	void Start () {
        bullets = new List<GameObject>();

        for(int i=0;i<amount;i++)
        {
            GameObject obj = Instantiate(bullet);
            obj.SetActive(false);
            bullets.Add(obj);


        }

        InvokeRepeating("Fire",0.5f, fireRate);
	}

    void Fire()
    {
        for(int i = 0;i < bullets.Count;i++)
        {
            if(!bullets[i].activeInHierarchy)
            {

                bullets[i].transform.position = transform.position;
                bullets[i].transform.rotation = transform.rotation;
                bullets[i].SetActive(true);
                break;


            }


        }



    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
