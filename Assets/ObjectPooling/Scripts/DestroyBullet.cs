﻿using UnityEngine;
using System.Collections;

public class DestroyBullet : MonoBehaviour {

    public int destroyTime = 3;
	// Use this for initialization
	void OnEnable()
    {
        Invoke("Pooling", destroyTime);
    }

    void Pooling()
    {
        gameObject.SetActive(false);
    }

    void Update()
    {
        transform.Translate(0, 0, 1f);
    }

    void OnDisable()
    {
        CancelInvoke();
    }
}
