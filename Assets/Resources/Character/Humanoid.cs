﻿using UnityEngine;
using System.Collections;

public class Humanoid : MonoBehaviour {

    public float speed = 3f;
    public int inventorySize = 10;

    protected virtual void Update()//un astfel de update pentru subclasa
    {
        Move();//apeleaza metoda Move();
    }

    public virtual void Move()//cuvantul "virtual"
    {
        float horiz = Input.GetAxisRaw("Horizontal") * Time.deltaTime;
        float vert = Input.GetAxisRaw("Vertical") * Time.deltaTime;
        transform.Translate(0f, 0f, vert * speed);
        transform.Rotate(Vector3.up * horiz * speed * 20f);
    }

}
