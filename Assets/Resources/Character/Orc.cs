﻿using UnityEngine;
using System.Collections;

//subclasa al clasei Humanoid
public class Orc : Humanoid {


    protected override void Update() 
    {
        base.Update();//apelam metoda Update() din clasa parinte(Humanoid)
        Jump();//apelam metoda Jump()

    }

    void Jump()
    {
        if(Input.GetButton("Jump"))
        {
         
                transform.Translate(0f, speed * Time.deltaTime, 0f);

        }
    }

	
}
