﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; //Pentru libraria de List.

public class CharacterSelection : MonoBehaviour {

    public GameObject button;
    public float torque = 1.2f; //Variabila pentru rotatie
    public List<GameObject> characterList;  //Lista de caractere
    int index = 0; //Caracterul curent

    // Use this for initialization
    void Start () {
        //Incarca intr-un Array toate obiectele din Resources/Prefabs
        GameObject[] characters = Resources.LoadAll<GameObject>("Prefabs"); 
        //Pentru fiecare obiect(variabila c ) din Array-ul characters
    foreach (GameObject c in characters) 
        {
            //Spawn ca si GameObject pentru am putea folosi mai tarziu obiectele
            GameObject chars = Instantiate(c,transform.position,Quaternion.identity) as GameObject;
            chars.transform.SetParent(gameObject.transform);//Seteaza ca si parinte acest obiect la care este atasat script-ul
            characterList.Add(chars);//Adauga obiectele spawnate in lista characterList
            chars.SetActive(false);//dezctiveaza toate caracterele
            characterList[index].SetActive(true);//activeaza primul caracter 

        }

    }
	
	// Update is called once per frame
	void Update () {
        characterList[index].transform.Rotate(Vector3.up * torque);//roteste caracterul curent
	}

    //buton pentru urmatorul caracter
    public void Next()
    {
        //dezactiveaza caracterul curent
        characterList[index].SetActive(false);
        if (index == characterList.Count - 1)//daca a ajuns la capul listei
        {
            index = 0;//atunci incepe de la capat
        }
        else
        {
            index++;//daca nu,mergi mai departe 
        }
        characterList[index].SetActive(true);//activeaza caracterul urmator
    }

    //buton pentru caracterul precedent
    public void Previous()
    {
        //dezactiveaza caracterul curent
        characterList[index].SetActive(false);
        if(index == 0)//daca a ajuns la inceputul listei
        index = characterList.Count - 1;//atunci incepe de la capul listei
        else
            index--;//daca nu,mergi mai departe
        characterList[index].SetActive(true);//activeaza caracterul urmator
    }

    //pentru butonul de selectie
    public void Select()
    {
        torque = 0f; //Nu se mai roteste
        button.SetActive(false); //dezactiveaza butoanele de selectie
    }


}
