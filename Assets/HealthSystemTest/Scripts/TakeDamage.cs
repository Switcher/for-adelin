﻿using UnityEngine;
using System.Collections;

public class TakeDamage : MonoBehaviour {

    HealthUI healthUI = new HealthUI();
    public int amount = 15;

    public void Damage()
    {
        healthUI.TakeDamage = amount;
        Debug.Log(healthUI.TakeDamage);
        
    }
}
