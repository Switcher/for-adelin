﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthUI : MonoBehaviour {

    public Slider sliderUI;
    public float startHealth = 100f;
    [SerializeField]
    private float currentHealth;
    public float decreaseRate = 42.5f;

	// Use this for initialization
	void Awake () {
        currentHealth = startHealth;
	}
    
    void Update()
    {
       
            sliderUI.value = Mathf.MoveTowards(sliderUI.value, currentHealth, Time.deltaTime * decreaseRate );
            
    }
    	
	// Update is called once per frame
	public float TakeDamage {

        get{
            return currentHealth;
        }
        set{ currentHealth = value - 15; }
       
        
    }
}
